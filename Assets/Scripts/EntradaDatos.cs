using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EntradaDatos : MonoBehaviour
{
  
    public string filename = "/StreamingAssets/Libros.json";
    public TMP_Text tituloL, autorL, paginasL;
    int index = 0;
    public List<Libros> libros = new List<Libros>();
    
    private void Start()
    {
        libros = Lectura.ReadFromJSON<Libros>(filename);
        index = libros.Count;
        tituloL.text ="T�tulo: " + libros[0].titulo;
        autorL.text = "Autor: " + libros[0].autor;
        paginasL.text = libros[0].paginas + " p�g.";
    }
   

    public void Retroceder()
    {
        if (index > 0)
        {
            index--;
            tituloL.text = "T�tulo: " + libros[index].titulo;
            autorL.text = "Autor: " + libros[index].autor;
            paginasL.text = libros[index].paginas + " p�g.";
        }
        else
        {
            index = 0; 
            tituloL.text = "T�tulo: " +  libros[index].titulo;
            autorL.text = "Autor: " + libros[index].autor;
            paginasL.text = libros[index].paginas + " p�g.";
        }
    }

    public void Avanzar()
    {
        if (index < libros.Count-1)
        {
            index++;
            tituloL.text = "T�tulo: " + libros[index].titulo;
            autorL.text = "Autor: " + libros[index].autor;
            paginasL.text = libros[index].paginas + " p�g.";
        }
        else
        {
            index = libros.Count - 1;
            tituloL.text = "T�tulo: " + libros[index].titulo;
            autorL.text = "Autor: " + libros[index].autor;
            paginasL.text = libros[index].paginas + " p�g.";
        }
    }

    public void Primero()
    {
        index=0;
        tituloL.text = "T�tulo: " +libros[index].titulo;
        autorL.text = "Autor: " + libros[index].autor;
        paginasL.text = libros[index].paginas + " p�g.";
    }

    public void Ultimo()
    {
        index = libros.Count-1; 
        tituloL.text = "T�tulo: " + libros[index].titulo;
        autorL.text = "Autor: " + libros[index].autor;
        paginasL.text = libros[index].paginas + " p�g.";
    }

    public void CrearLibros()
    {
        SceneManager.LoadScene(4);
    }

    public void salir()
    {
        Application.Quit();
        Debug.Log("Salir de la APP");
    }
}
