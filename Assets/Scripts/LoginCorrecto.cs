using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoginCorrecto : MonoBehaviour
{
    public TMP_Text usuario;
    void Start()
    {
        usuario.text = "Bienvenido, " + PlayerPrefs.GetString("username") + ".\n" +
            "Ahora puede consultar nuestros libros";
    }

    public void Consultar()
    {
        SceneManager.LoadScene(3);
    }
}
