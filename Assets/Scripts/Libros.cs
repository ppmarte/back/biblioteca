using System;
using System.Collections.Generic;

[System.Serializable]
public class Libros
{
    public string titulo;
    public string autor;
    public string paginas;

    public Libros(string titulo, string autor, string paginas)
    {
        this.titulo = titulo;
        this.autor = autor;
        this.paginas = paginas;
    }
}


