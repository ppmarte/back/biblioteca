using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    public TMP_Text texto;
    public TMP_InputField userName, pass;
    string nombre = "paola", password = "12345";
    public int intentos = 3;
    public void Acceder()
    {
        
        if (userName.text.Equals(nombre) && pass.text.Equals(password) && intentos >0)
        {
            PlayerPrefs.SetString("username", userName.text);
            SceneManager.LoadScene(2);
        }
        else
        {
            
            intentos--;
            if (intentos == 0)
            {
                texto.text = "Has agotado el n�mero de intentos";
                PlayerPrefs.SetString("mensaje", texto.text);
                SceneManager.LoadScene(1);
            }
            texto.text = "Usuario Incorrecto. Te quedan " + intentos+ " intentos";
            
        }

        

    }

    public void salir()
    {
        Application.Quit();
        Debug.Log("Salir de la APP");
    }
}
