using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FalloIntento : MonoBehaviour
{
    public TMP_Text usuario;
    void Start()
    {
        usuario.text = "Lo sentimos, " + PlayerPrefs.GetString("mensaje");
    }

    public void salir()
    {
        Application.Quit();
        Debug.Log("Salir de la APP");
    }
}