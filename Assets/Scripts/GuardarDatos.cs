using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.SceneManagement;
using static UnityEditor.PlayerSettings;


public class GuardarDatos : MonoBehaviour
{

    public List<Libros> libros = new List<Libros>();
    public TMP_InputField titulo;
    public TMP_InputField autor;
    public TMP_InputField paginas;
    public TMP_Text mensaje ;


    public string filename = "/StreamingAssets/Libros.json";
    public void Start()
    {
        libros = Lectura.ReadFromJSON<Libros>(filename);
    }
    public void AddLibro()
    {
        libros.Add(new Libros(titulo.text, autor.text, paginas.text));
        titulo.text = " ";
        autor.text = " ";
        paginas.text = " ";
        mensaje.text = "Se ha agregado correctamente";
        
        Lectura.SaveToJSON<Libros>(libros, filename);
        mensaje.text = " ";
    }
    public void Volver()
    {
        SceneManager.LoadScene(3);
    }

    public void salir()
    {
        Application.Quit();
        Debug.Log("Salir de la APP");
    }
}
